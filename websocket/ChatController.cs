﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Microsoft.Web.WebSockets;

namespace websocket
{
    public class ChatController:ApiController
    {
        private readonly ChatService _service;

        public ChatController()
        {
            _service = new ChatService();
        }
        public HttpResponseMessage Get(string username)
        {
            if (HttpContext.Current.IsWebSocketRequest)
            {
                HttpContext.Current.AcceptWebSocketRequest(new ChatWebSocketHandler("asdf"));
                return Request.CreateResponse(HttpStatusCode.SwitchingProtocols);
            }
            return new HttpResponseMessage(HttpStatusCode.BadRequest);
        }
        [HttpGet]
        public HttpResponseMessage Message([FromUri]string message)
        {
            var result = _service.Message(message);
            return result ? new HttpResponseMessage(HttpStatusCode.OK) : new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}