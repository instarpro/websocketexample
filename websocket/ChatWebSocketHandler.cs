using System.Collections.Generic;
using Microsoft.Web.WebSockets;

namespace websocket
{
    public class ChatWebSocketHandler:WebSocketHandler
    {
        private readonly string _id;
        public static readonly IDictionary<string,WebSocketCollection> Clients = new Dictionary<string, WebSocketCollection>();
        public ChatWebSocketHandler(string id)
        {
            _id = id;
        }

        public override void OnOpen()
        {
            if (Clients.ContainsKey(_id))
            {
                Clients[_id].Add(this);
            }
            else
            {
                Clients.Add(new KeyValuePair<string, WebSocketCollection>(_id,new WebSocketCollection{this}));
            }
           
        }
    }
}