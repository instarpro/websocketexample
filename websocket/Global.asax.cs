﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;


namespace websocket
{
    public class MvcApplication:HttpApplication
    {
        protected void Application_Start()
        {
            RouteConfig.RegisterRoutes(GlobalConfiguration.Configuration);
        }

    }

    public class RouteConfig
    {
        public static void RegisterRoutes(HttpConfiguration config)
        {
           config.Routes.MapHttpRoute(
               name: "ControllerAndActionAndId",
               routeTemplate: "{controller}/{action}",
               defaults: new { id = RouteParameter.Optional }
           );

            //Возвращаем всегда в json
            config.Formatters.Remove(
                GlobalConfiguration.Configuration.Formatters.XmlFormatter);
        }
    }
}